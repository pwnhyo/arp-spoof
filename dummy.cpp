	EthArpPacket packet;

	packet.eth_.dmac_ = Mac("46:f2:1b:42:d1:64");
	packet.eth_.smac_ = Mac("00:1c:42:fb:fc:9f");
	packet.eth_.type_ = htons(EthHdr::Arp);

	packet.arp_.hrd_ = htons(ArpHdr::ETHER);
	packet.arp_.pro_ = htons(EthHdr::Ip4);
	packet.arp_.hln_ = Mac::SIZE;
	packet.arp_.pln_ = Ip::SIZE;
	packet.arp_.op_ = htons(ArpHdr::Request);
	packet.arp_.smac_ = Mac("00:1c:42:fb:fc:9f");
	packet.arp_.sip_ = htonl(Ip("172.20.10.14"));
	packet.arp_.tmac_ = Mac("46:f2:1b:42:d1:64");
	packet.arp_.tip_ = htonl(Ip("172.20.10.1"));

	int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
	if (res != 0) {
		fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
	}


    